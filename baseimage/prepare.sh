#!/bin/bash

echo "Install essential application"
apt-get install -y --no-install-recommends language-pack-ko language-pack-ko-base 

echo "Set Timezone"
DEBIAN_FRONTEND=noninteractive apt-get install -y tzdata
ln -fs /usr/share/zoneinfo/Asia/Seoul /etc/localtime
